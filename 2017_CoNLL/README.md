### Read me
- Paper list: [CoNLL accepted 2017](http://www.conll.org/accepted-2017)
- The following list is in reverse chronological order

### 2018-03-29
- [Zero-Shot Relation Extraction via Reading Comprehension](https://arxiv.org/pdf/1706.04115.pdf)
- [Feature Selection as Causal Inference: Experiments with Text Classification](https://www.aclweb.org/anthology/K/K17/K17-1018.pdf)

### 2018-03-28
- [Named Entity Disambiguiation for Noisy Text](http://www.aclweb.org/anthology/K17-1008)
- [Learning Word Representations with Regularization from Prior Knowledge](https://www.aclweb.org/anthology/K/K17/K17-1016.pdf)

### 2018-03-27
- [A Supervised Approach to Extractive Summarisation of Scientific Papers](http://www.aclweb.org/anthology/K17-1021)
- [An Artificial Language Evaluation of Distributional Semantic Models](http://www.aclweb.org/anthology/K17-1015)

### 2018-03-26
- [A Probabilistic Generative Grammar for Semantic Parsing](http://www.aclweb.org/anthology/K17-1026)
- [A Simple and Accurate Syntax-Agnostic Neural Model for Dependency-based Semantic Role Labeling](http://www.aclweb.org/anthology/K17-1041)

